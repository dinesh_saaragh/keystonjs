var FieldType = require('../Type');
var TextType = require('../text/TextType');
var util = require('util');


/**
 * Threejs FieldType Constructor
 * @extends Field
 * @api public
 */
function threejs (list, path, options) {
	this._nativeType = String;
	this._defaultSize = 'full';
	this.wysiwyg = options.wysiwyg || false;
	this.height = options.height || 180;
	this._properties = ['wysiwyg', 'height'];
	threejs.super_.call(this, list, path, options);
}
threejs.properName = 'Threejs';
util.inherits(threejs, FieldType);


threejs.prototype.validateInput = TextType.prototype.validateInput;
threejs.prototype.validateRequiredInput = TextType.prototype.validateRequiredInput;

/* Inherit from TextType prototype */
threejs.prototype.addFilterToQuery = TextType.prototype.addFilterToQuery;

/* Export Field Type */
module.exports = threejs;
