var FieldType = require('../Type');
var TextType = require('../text/TextType');
var util = require('util');
var utils = require('keystone-utils');


/**
 * Text FieldType Constructor
 * @extends Field
 * @api public
 */
function textarea (list, path, options) {
	this.options = options;
	this._nativeType = String;
	this._underscoreMethods = ['format', 'crop'];
	this.height = options.height || 90;
	this.multiline = true;
	this.placeholder = options.placeholder;
	this._properties = ['height', 'multiline'];
	textarea.super_.call(this, list, path, options);
}
textarea.properName = 'Textarea';
util.inherits(textarea, FieldType);

// textarea.prototype.validateInput = TextType.prototype.validateInput;
textarea.prototype.validateInput = function (data, callback) {
	var detail = '';
	var max = this.options.max;
	var min = this.options.min;
	var value = this.getValueFromData(data);
	var key = TextType.prototype.getKeyByValue(data, value);
	var result = value === undefined || value === null || typeof value === 'string';
	if (max && typeof value === 'string') {
		if (value.length > max) {
			result = false;
			detail += utils.upcase(key) + ' should not be more than ' + max + ' characters.';
		}
	}
	if (min && typeof value === 'string') {
		if (value.length < min) {
			result = false;
			detail += ' ' +  utils.upcase(key) + ' should not be less than ' + min + ' characters.';
		}
	}

	utils.defer(callback, result, detail.trim());
};

textarea.prototype.validateRequiredInput = TextType.prototype.validateRequiredInput;

/* Inherit from TextType prototype */
textarea.prototype.addFilterToQuery = TextType.prototype.addFilterToQuery;
textarea.prototype.crop = TextType.prototype.crop;

/**
 * Formats the field value
 * @api public
 */
textarea.prototype.format = function (item) {
	return utils.textToHTML(item.get(this.path));
};

/* Export Field Type */
module.exports = textarea;
