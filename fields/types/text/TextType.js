var FieldType = require('../Type');
var util = require('util');
var utils = require('keystone-utils');
var _ = require('lodash');

/**
 * Text FieldType Constructor
 * @extends Field
 * @api public
 */
function text (list, path, options) {
	this.options = options;
	this._nativeType = String;
	this._properties = ['monospace'];
	this._underscoreMethods = ['crop'];
	text.super_.call(this, list, path, options);
}
text.properName = 'Text';
util.inherits(text, FieldType);

text.prototype.validateInput = function (data, callback) {
	var detail = '';
	var max = this.options.max;
	var min = this.options.min;
	var nospaces = this.options.nospaces;
	var value = this.getValueFromData(data);
	var key = getKeyByValue(data, value);
	var result = value === undefined || value === null || typeof value === 'string';
	if (max && typeof value === 'string') {
		// result = value.length < max;
		if (value.length > max) {
			result = false;
			detail += utils.upcase(_.startCase(key)) + ' should not be more than ' + max + ' characters.';
		}
	}
	if (min && typeof value === 'string') {
		// result = value.length > min;
		if (value.length < min) {
			result = false;
			detail += ' ' +  utils.upcase(_.startCase(key)) + ' should not be less than ' + min + ' characters.';
		}
	}
	if (nospaces && /\s/g.test(value)) {
		result = false;
		detail += ' ' +  utils.upcase(_.startCase(key)) + ' should not contain spaces.';
	}

	// utils.defer(callback, result);
	utils.defer(callback, result, detail.trim());
};

text.prototype.validateRequiredInput = function (item, data, callback) {
	var value = this.getValueFromData(data);
	var result = !!value;
	if (value === undefined && item.get(this.path)) {
		result = true;
	}
	utils.defer(callback, result);
};

/**
 * Add filters to a query
 */
text.prototype.addFilterToQuery = function (filter) {
	var query = {};
	if (filter.mode === 'exactly' && !filter.value) {
		query[this.path] = filter.inverted ? { $nin: ['', null] } : { $in: ['', null] };
		return query;
	}
	var value = utils.escapeRegExp(filter.value);
	if (filter.mode === 'beginsWith') {
		value = '^' + value;
	} else if (filter.mode === 'endsWith') {
		value = value + '$';
	} else if (filter.mode === 'exactly') {
		value = '^' + value + '$';
	}
	value = new RegExp(value, filter.caseSensitive ? '' : 'i');
	query[this.path] = filter.inverted ? { $not: value } : value;
	return query;
};

/**
 * Crops the string to the specifed length.
 */
text.prototype.crop = function (item, length, append, preserveWords) {
	return utils.cropString(item.get(this.path), length, append, preserveWords);
};

/**
 * Get the key name of the object from value
 */
text.prototype.getKeyByValue = getKeyByValue;

/* Export Field Type */
module.exports = text;

function getKeyByValue (object, value) {
	return Object.keys(object).find(key => object[key] === value);
}
