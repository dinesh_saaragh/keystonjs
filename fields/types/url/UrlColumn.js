import React from 'react';
import ItemsTableCell from '../../components/ItemsTableCell';
import ItemsTableValue from '../../components/ItemsTableValue';

var UrlColumn = React.createClass({
	displayName: 'UrlColumn',
	propTypes: {
		col: React.PropTypes.object,
		data: React.PropTypes.object,
	},
	renderValue () {
		var value = this.props.data.fields[this.props.col.path];
		var fixedUrl = false;
		
		if (!value) {
			var extUrl = this.props.col.path.split(":");
			if(extUrl[0] === 'url' && extUrl.length == 4) {
				value = `${Keystone.adminPath}/`+extUrl[1]+'?filters=%5B%7B"path"%3A"'+extUrl[2]+'"%2C"inverted"%3Afalse%2C"value"%3A%5B"' + this.props.data.id + '"%5D%7D%5D';
				fixedUrl = true;
			}	
		}
		if (!value) return;

		// if the value doesn't start with a prototcol, assume http for the href
		var href = value;
		if (href && !/^(mailto\:)|(\w+\:\/\/)/.test(href)) {
			href = 'http://' + value;
		}

		// strip the protocol from the link if it's http(s)
		var label = value.replace(/^https?\:\/\//i, '');

		if(fixedUrl) {
			href=value;
			label='View';
		}

		return (
			<ItemsTableValue to={href} padded exterior field={this.props.col.type}>
				{label}
			</ItemsTableValue>
		);
	},
	render () {
		return (
			<ItemsTableCell>
				{this.renderValue()}
			</ItemsTableCell>
		);
	},
});

module.exports = UrlColumn;
