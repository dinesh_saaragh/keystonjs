var _ = require('lodash');

/**
 * Retrieves orphaned lists (those not in a nav section)
 */

function getAdminLists () {
	if (!this.nav) {
		return [];
	}

	return _.filter(this.lists, function (list, key) {
		return (list.get('adminonly')) ? list : false;
	}.bind(this));
}

module.exports = getAdminLists;
