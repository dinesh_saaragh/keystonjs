var multer = require('multer');
var bodyParser = require('body-parser');
var os = require('os');

module.exports = function bindIPRestrictions (keystone, app) {
	// Set up body options and cookie parser
	var bodyParserParams = {};
	if (keystone.get('file limit')) {
		bodyParserParams.limit = keystone.get('file limit');
	}
	var upload = new multer({ dest: os.tmpdir(), includeEmptyFields: true });
	app.use(bodyParser.json(bodyParserParams));
	bodyParserParams.extended = true;
	app.use(bodyParser.urlencoded(bodyParserParams));
	app.use(upload.any());
};
