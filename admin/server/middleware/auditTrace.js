const _ = require('lodash');

module.exports = function (req, res, next) {
	res.auditTrace = function auditTrace (endpoint, description, modelName, itemid, err) {

		var auditTraceConfig = req.keystone.get('audittrace');

		if (auditTraceConfig === true) {

			var keystone = req.keystone;
			var audittrace_list = keystone.list('audit-traces');
			var audittrace = audittrace_list.model();

			if (err && typeof (err) === 'object') {
				err = JSON.stringify(err);
			}

			/** parse again the description and see if there is password or password_confirm values */
			description = JSON.parse(description);
			if (_.has(description, 'password')) {
				// masked the password value
				description.password = '******';
			}

			if (_.has(description, 'password_confirm')) {
				// masked the password_confirm value
				description.password_confirm = '******';
			}

			var logdata = {
				userid: req.user.userid,
				path: endpoint,
				action: req.method,
				data: JSON.stringify(description),
				error: err,
				devicelog: req.headers['user-agent'],
			};

			if (modelName === 'User') {
				logdata.user = itemid;
			} else if (modelName === 'Patient') {
				logdata.patient = itemid;
			} else if (modelName === 'Device') {
				logdata.device = itemid;
			} else if (modelName === 'Form') {
				logdata.form = itemid;
			} else if (modelName === 'PDFReport') {
				logdata.pdfreport = itemid;
			} else if (modelName === 'WoundGraph') {
				logdata.woundgraph = itemid;
			}

			audittrace_list.updateItem(audittrace, logdata, {
				ignoreNoEdit: true,
				user: req.user,
			}, function (err) {

				if (err) {
					var status = err.error === 'validation errors' ? 400 : 500;
					var error = err.error === 'database error' ? err.detail : err;
					console.log('audittrace status! ', status);
					console.log('audittrace error! ', error);
					console.log('Details: ' + req.user.userid + ' - ' + endpoint);
				}
			});
		}
	};
	next();
};
