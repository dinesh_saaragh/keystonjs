const _ = require('lodash');

module.exports = function (req, res, next) {
	res.auditLog = function auditLog (endpoint, description, err) {

		var auditLogConfig = req.keystone.get('auditLog');

		if (auditLogConfig === true) {

			var keystone = req.keystone;
			var auditlog_list = keystone.list('audit-logs');
			var auditlog = auditlog_list.model();

			if (err && typeof (err) === 'object') {
				err = JSON.stringify(err);
			}

			/** parse again the description and see if there is password or password_confirm values */
			description = JSON.parse(description);
			if (_.has(description, 'password')) {
				// masked the password value
				description.password = '******';
			}

			if (_.has(description, 'password_confirm')) {
				// masked the password_confirm value
				description.password_confirm = '******';
			}

			var logdata = {
				userid: req.user.userid,
				path: endpoint,
				action: req.method,
				data: JSON.stringify(description),
				error: err,
				device: req.headers['user-agent'],
			};
			auditlog_list.updateItem(auditlog, logdata, {
				ignoreNoEdit: true,
				user: req.user,
			}, function (err) {

				if (err) {
					var status = err.error === 'validation errors' ? 400 : 500;
					var error = err.error === 'database error' ? err.detail : err;
					console.log('AuditLog error! ', error);
					console.log('Details: ' + req.user.userid + ' - ' + endpoint);
				}
			});
		}
	};
	next();
};
