var utils = require('keystone-utils');
var session = require('../../../../lib/session');
var moment = require('moment');
var ActiveDirectory = require('activedirectory');

function signin (req, res) {
	var keystone = req.keystone;
	var adConfig = keystone.get('AD config');
	const security_settings = keystone.get('security settings');
	const numFailedLoginAttempts = security_settings.max_failed_login_attempts;

	if (!keystone.security.csrf.validate(req)) {
		return res.apiError(403, 'invalid csrf');
	}
	if (!req.body.email || !req.body.password) {
		return res.status(401).json({ error: 'email and password required' });
	}

	// initialized the login attempt
	if (!req.session.login_attempt) req.session.login_attempt = 0;

	var User = keystone.list(keystone.get('user model'));
	var emailRegExp = new RegExp('^' + utils.escapeRegExp(req.body.email) + '$', 'i');
	User.model.findOne({ $or: [{ email: emailRegExp }, { userid: emailRegExp }] }).exec(function (err, user) {
		if (user) {
			// check if exceed_login_attempt is enabled
			if (user.exceed_login_attempt) {
				return res.status(401).json({ error: 'login attempts exceeded' });
			}

			// authenticate with AD user
			if (adConfig && user.isADUser) {
				keystone.callHook(user, 'pre:signin', req, function (err) {
					if (err) return res.status(500).json({ error: 'pre:signin error', detail: err });
					let activeDir = new ActiveDirectory(adConfig);

					activeDir.authenticate(user.email, req.body.password, function (err, auth) {
						if (err) {
							checkLoginAttempts(req, user, numFailedLoginAttempts);
							// invalid AD credential
							return res.status(401).json({ error: 'AD user invalid credential error' });
						}
						// authenticated AD user
						session.signinWithUser(user, req, res, function () {
							keystone.callHook(user, 'post:signin', req, function (err) {
								if (err) {
									checkLoginAttempts(req, user, numFailedLoginAttempts);
									return res.status(500).json({ error: 'post:signin error', detail: err });
								}
								// successful login clear the login attempts
								req.session.login_attempt = null;
								res.json({ success: true, user: user });
							});
						});
					});
				});
			}
			else {
				keystone.callHook(user, 'pre:signin', req, function (err) {
					if (err) return res.status(500).json({ error: 'pre:signin error', detail: err });
					user._.password.compare(req.body.password, function (err, isMatch) {
						if (isMatch) {
							// password is match
							// but need to check if password expiration date is expired
							if (user.password_expires) {
								let today = moment().format('YYYY-MM-DD');
								let passwordExpires = moment(user.password_expires).format('YYYY-MM-DD');

								if (moment(today).isAfter(passwordExpires)) {
									return res.status(401).json({ error: 'expired password' });
								}
							}

							session.signinWithUser(user, req, res, function () {
								keystone.callHook(user, 'post:signin', req, function (err) {
									if (err) {
										checkLoginAttempts(req, user, numFailedLoginAttempts);
										return res.status(500).json({ error: 'post:signin error', detail: err });
									}
									// successful login clear the login attempts
									req.session.login_attempt = null;
									res.json({ success: true, user: user });
								});
							});
						} else if (err) {
							checkLoginAttempts(req, user, numFailedLoginAttempts);
							return res.status(500).json({ error: 'bcrypt error', detail: err });
						} else {
							checkLoginAttempts(req, user, numFailedLoginAttempts);
							return res.status(401).json({ error: 'invalid details' });
						}
					});
				});
			}

		} else if (err) {
			return res.status(500).json({ error: 'database error', detail: err });
		} else {
			return res.status(401).json({ error: 'invalid details' });
		}
	});
}

module.exports = signin;

function checkLoginAttempts(req, user, numFailedLoginAttempts) {
	req.session.login_attempt++;
	// check the login attempts
	if (req.session.login_attempt >= numFailedLoginAttempts) {
		user.exceed_login_attempt = true;
		user.save();
	}
}
