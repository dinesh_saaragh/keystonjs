var async = require('async');

module.exports = function (req, res) {
	var keystone = req.keystone;
	var counts = {};

	res.header("Expires", "-1");
	res.header("Cache-Control", "no-cache, no-store, private");
		
	async.each(keystone.lists, function (list, next) {
		list.model.countDocuments(function (err, count) {
			counts[list.key] = count;
			next(err);
		});
	}, function (err) {
		if (err) return res.apiError('database error', err);
		return res.json({
			counts: counts,
		});
	});
};
