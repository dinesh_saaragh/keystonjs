
const CSV = require('fast-csv');
const csvHeaders = require('csv-headers');

const async = require('async');
const _ = require('lodash');
const fs = require('fs');
//const S = require('string');

module.exports = function(req, res) {

	var requiredFields = req.body.requiredFields; // this can be empty
	var allowedFields = req.body.allowedFields.split(',');

	if (_.isEmpty(requiredFields)) {
		requiredFields = allowedFields;
	}
	else {
		requiredFields = requiredFields.split(',');
	}

	var csvFileName = req.files.csvfile.path;

	// extract the headers from csv file
	csvHeaders({ file: csvFileName }, (err, headers) => {
		if (err) {
			console.error('csvHeaders err:', err);
			return res.apiError(400, { error: 'CSV file have no headers' });
		}

		var notAllReqFieldsExist = checkRequiredFieldsExists(headers, requiredFields);
		if (notAllReqFieldsExist) {
			fs.unlinkSync(csvFileName);
			return res.apiError(400, { error: `CSV field should have [ ${requiredFields.join(', ')} ]` });
		}

		var itemObjArr = []; // array of object holder - csv.on('data')
		var lineNum = 2; // headers included, so line num starts at 2

		CSV
			.fromPath(csvFileName, { headers: true })
			.on('data', csvrow => {
				var itemObj = {};

				// collect all the allowed fields in each row
				Object.keys(csvrow).forEach(key => {
					if (_.includes(allowedFields, key)) {
						itemObj[key] = csvrow[key];
					}
				});

				itemObj._line = lineNum++;
				itemObjArr.push(itemObj);
			})
			.on('end', () => {
				insertDataToDB(req, res, itemObjArr, csvFileName);
			})
			.on('error', err => {
				console.error('uploading - onerror:', err);
				err.error = err.message;
				return res.apiError(400, err);
			});

	});

};

function insertDataToDB (req, res, itemObjArr, csvFileName) {
	// delete the csv file
	fs.unlinkSync(csvFileName);

	// for example having 1000 csv rows, instead of uploading all 1000 rows in one shot
	// will split the uploading define by this batch size
	var batchSize = req.keystone.get('csvupload batch size') || 100;

	var itemsObjToSave;

	// async.whilst - similar with while loop but uses async
	async.whilst(
		function () {
			itemsObjToSave = itemObjArr.splice(0, batchSize);
			// check itemsObjToSave doest have elements to save
			return itemsObjToSave.length > 0;
		},
		function (callback) {
			async.eachSeries(itemsObjToSave,
				function (item, cb) {
					var itemModel = new req.list.model();

					Object.keys(item).forEach(key => {
						itemModel[key] = item[key];
					});

					// save the tracking fields
					// customized tracking fields
					itemModel['created_by'] = req.user._id;
					itemModel['last_modified_by'] = req.user._id;
					// default tracking fields
					itemModel['createdBy'] = req.user._id;
					itemModel['updatedBy'] = req.user._id;

					// save the data one by one
					itemModel.save((_err, doc) => {
						if (_err) {
							// include the csv line num in the error message
							var appendText = `at CSV file line ${item._line}`;
							_err.message = `${_err.message} ${appendText}`;

							if (_err.code == 11000) { // duplicate error
								// expecting error message: `MongoError: E11000 duplicate key error collection: guestgenie2.items index: name_1 dup key: { : "Burger" }`
								var fieldname = _err.message.match(/[a-zA-z]*_1/)[0].replace('_1', '');
								var fieldvalue = _err.message.match(/\{([^)]+)\}/)[0];
								fieldvalue = S(fieldvalue).stripPunctuation().s.trim();

								_err.message = `Duplicate field value error { ${fieldname}: ${fieldvalue} } ${appendText}`;
							}

							if (item._line > 2) {
								// specify lines that are successfully uploaded, before error encountered
								_err.message = `${_err.message}. But lines 2 - ${item._line - 1} are successfully uploaded`;
							}
							// if error throw will stop on specified line
							return cb(_err);
						}

						cb();
					});
				},
				function (err) {
					if (err) {
						// return the error to async.while to stop the iteration
						// and display error on specified line
						return callback(err);
					}

					// success async.eachSeries
					// proceed to next async.whilst iteration if there's any
					callback();
				}
			); // end async.eachSeries
		},
		function (err) {
			if (err) {
				console.error('error:', err.message);
				err.error = err.message;
				return res.apiError(400, err);
			}
			console.log('=== end: success upload ===');
			// success async.whilst
			res.json({ success: true, items: 'ok' });
		}
	); // end async.whilst

}

function checkRequiredFieldsExists (headers, _requiredFields) {
	var fieldExistList = [];

	for (var field of _requiredFields) {
		// initialize the element as false (field is not in headers)
		var el = false;

		if (_.includes(headers, field)) {
			// field is in the headers
			el = true;
		}

		fieldExistList.push(el);
	}

	return _.includes(fieldExistList, false);
}
