/**
 * The form that's visible when "Create <ItemName>" is clicked on either the
 * List screen or the Item screen
 */

import React from 'react';
import vkey from 'vkey';
import AlertMessages from './AlertMessages';
import { Fields } from 'FieldTypes';
import { Button, Center, Form, Modal, Spinner } from '../elemental';
import _ from 'lodash';
import theme from "../../theme";


const UploadForm = React.createClass({
	displayName: 'UploadForm',
	propTypes: {
		err: React.PropTypes.object,
		isOpen: React.PropTypes.bool,
		list: React.PropTypes.object,
		onCancel: React.PropTypes.func,
		onUpload: React.PropTypes.func,
		onClose: React.PropTypes.func,
	},
	getDefaultProps () {
		return {
			err: null,
			isOpen: false,
		};
	},
	getInitialState () {
		// Set the fields required for CSV file to upload for each model
		// if CSV file uploaded doesnt have the required fields, will throw error

		var requiredFields = [];
		var allowedFields = [];

		var list = this.props.list;
		var allowedTypes = ['text', 'number', 'relationship', 'select'];

		Object.keys(list.fields).forEach(key => {
			var field = list.fields[key];
			// console.log('fields ===>>', field);

			if (field.required || field.initial) {
				// fields to check to CSV file
				requiredFields.push(field.path);
			}

			if (field.required || field.initial || (_.includes(allowedTypes, field.type) && !field.noedit && !field.hidden)) {
				// fields show on the updload popup
				allowedFields.push(field.path);
			}
		});

		return {
			requiredFields: requiredFields,
			allowedFields: allowedFields,
			alerts: {},
			loading: false
		};
	},
	componentDidMount () {
		document.body.addEventListener('keyup', this.handleKeyPress, false);
	},
	componentWillUnmount () {
		document.body.removeEventListener('keyup', this.handleKeyPress, false);
	},
	handleKeyPress (evt) {
		if (vkey[evt.keyCode] === '<escape>') {
			this.props.onCancel();
		}
	},
	// Create a new item when the form is submitted
	submitForm (event) {
		event.preventDefault();
		var csvfile = document.getElementById('csvFileUpload').files[0];
		// display spinner
		this.setState({ loading: true });

		if (!csvfile) {
			var err = { error: 'No CSV file uploaded' };
			this.setState({
				alerts: { error: err },
				loading: false
			});
		}
		else {
			const formData = new FormData();
			formData.append('csvfile', csvfile);
			formData.append('requiredFields', this.state.requiredFields.join());
			formData.append('allowedFields', this.state.allowedFields.join());

			this.props.list.uploadItems(formData, (err, data) => {
				if (data) {
					// clear the alerts
					this.setState({
						alerts: {},
						loading: false
					});

					// success upload
					if (this.props.onUpload) {
						this.props.onUpload();
					}
				} else {
					if (!err) {
						err = { error: 'connection error' };
					}
					// If we get a database error, show the database error message
					// instead of only saying "Database error"
					if (err.error === 'database error') {
						err.error = err.detail.errmsg;
					}
					this.setState({
						alerts: { error: err },
						loading: false
					});
				}
			});
		}
	},
	// Reset the form and Close the Modal
	resetForm () {
		this.setState ({
			alerts: {},
			loading: false
		});

		this.props.onCancel();
	},
	// Render the form itself
	renderForm () {
		if (!this.props.isOpen) return;

		var list = this.props.list;
		var allowedFields = this.state.allowedFields;

		return (
			<Form layout="horizontal" onSubmit={this.submitForm} encType="multipart/form-data">
				<Modal.Header
					text={list.uploadlabel + ' ' + list.plural}
					showCloseButton
				/>
				<Modal.Body>
					<AlertMessages alerts={this.state.alerts} />

					<div style={{ margin: '0 0 10px' }}>
						<div style={{ padding: '10px 0' }}>CSV Allowed Fields: [ {allowedFields.join(', ')} ]</div>
						<div>
							<label for="csvFileUpload">CSV Upload</label>
							<input id="csvFileUpload" type="file" accept=".csv" style={{ marginLeft: '15px', cursor: 'pointer' }}/>
						</div>
					</div>

				</Modal.Body>
				<Modal.Footer>
					<Button color="success" type="submit" data-button-type="submit">{list.uploadlabel}</Button>
					<Button
						variant="link"
						color="cancel"
						data-button-type="cancel"
						onClick={this.resetForm}
					>
					Cancel
					</Button>
				</Modal.Footer>
			</Form>
		);
	},
	render () {
		return (
			<Modal.Dialog
				isOpen={this.props.isOpen}
				onClose={this.resetForm}
				backdropClosesModal={this.resetForm}
			>
				{this.renderForm()}
				{this.state.loading && <Center height="10vh">
					<Spinner />
				</Center>}
			</Modal.Dialog>
		);
	}
});

module.exports = UploadForm;
